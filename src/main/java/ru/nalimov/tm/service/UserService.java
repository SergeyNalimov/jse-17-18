package ru.nalimov.tm.service;

import ru.nalimov.tm.Application;
import ru.nalimov.tm.entity.User;
import ru.nalimov.tm.repository.UserRepository;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.util.HashMD5;

import java.util.*;

public class UserService extends AbstractController {

    private static final UserRepository userRepository = UserRepository.getInstance();

    private final List<User> users = new ArrayList<>();

    private Long userAuthenticId;
    private Long userDeauthentic;

    private User appUser;

    public User getAppUser() {
        return appUser;
    }

    public void setAppUser(User appUser) {
        this.appUser = appUser;
    }

    private static UserService instance = null;

    public UserService() {
    }

    public UserService(UserRepository userRepository) {

    }

    public static UserService getInstance() {
        synchronized (UserService.class) {
            return instance == null
                    ? instance = new UserService()
                    : instance;
        }
    }

    public User create(
            final String login, final String userPassword, final String firstName,
            final String secondName, final String lastName
    ){
        final User user = new User(login, userPassword, firstName, secondName, lastName);
        users.add(user);
        return user;
    }

    public User create(
            final String login, final String userPassword, final String firstName, final String secondName,
            final String lastName, Role userRole
    ){
        final User user = new User(login, userPassword, firstName, secondName, lastName, userRole);
        users.add(user);
        return user;
    }

    public int createUser
            (
                    final String login, final String userPassword, final String firstName, final String secondName,
                    final String lastName, Role userRole
            ) {
        create(login, userPassword, firstName, secondName, lastName, userRole);
        return 0;
    }

    public User findByLogin(final String login) {
        for (final User user: users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateByLogin(
            final String login, final String userPassword, final String firstName, final String secondName,
            final String lastName
    ){
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setLastName(lastName);
        return user;
    }

    public User findById(final Long id) {
        if (id == null) return null;
        for (final User user: users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    public User removeById(final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateById(
            final Long id, final String login, final String userPassword, final String firstName,
            final String secondName, final String lastName
    ) {
        final User user = findById(id);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setLastName(lastName);
        return user;
    }

    public void clear(){
        users.clear();
    }

    public int getSize() {
        return users.size();
    }

    public List<User> findAll()  {
        return users;
    }




    public int userAuthentic(){
        System.out.println("[AUTHENTIC]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLAESE, ENTER PASSWORD:");
        final String password = scanner.nextLine();
        final User user = userRepository.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        if (UserRepository.getInstance().userAuthentic(login, password) == null){
            System.out.println("[FAIL]");
            return 0;
        }
        setAppUser(user);
        return 0;
    }

    public int userDeauthentic(){
        System.out.println("[DEAUTHENTIC]");
        userDeauthentic();
        System.out.println("[OK]");
        return 0;
    }

    public int userChangePassword(){
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER OLD PASSWORD");
        final String oldPassword = scanner.nextLine();
        System.out.println("PLEASE, ENTER NEW PASSWORD");
        final String newPassword = scanner.nextLine();
        final User user = userRepository.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        if (!user.getHashPassword().equals(HashMD5.getHash(oldPassword))) {
            System.out.println("[FAIL]");
            return 0;
        }
        user.setHashPassword(newPassword);
        System.out.println("[OK]");
        return 0;
    }






//    public User updateProfile(Long id, String login, String firstname, String secondname, String lastname) {
//        if (login == null || login.isEmpty()) return null;
//        return userRepository.updateProfile(id, login, firstname, secondname, lastname);
//    }
//
//    public User updateRole(String login, String role) {
//        if (login == null || login.isEmpty()) return null;
//        if (role == null || role.isEmpty()) return null;
//        return userRepository.updateRole(login, role);
//    }

    public int createUser() {
        System.out.println("[CREATE STANDART USER]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SECOND NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String lastName = scanner.nextLine();
        userRepository.create(login, password, firstName, secondName, lastName);
        System.out.println("[OK]");
        return 0;
    }

    public int createUser(Role userRole) {
        System.out.println("[CREATE ADMIN]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SECOND NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String lastName = scanner.nextLine();
        userRepository.create(login, password, firstName, secondName, lastName, userRole);
        System.out.println("[OK]");
        return 0;
    }
    public int updateProfile(final Long id) {
        final String login = isLoginExists();
        if (login == null) {
            return -1;
        }
        else {
            System.out.println("PLEASE, ENTER YOUR NEW FIRSTNAME:");
            final String firstname = scanner.nextLine();
            System.out.println("PLEASE, ENTER YOUR NEW SECONDNAME:");
            final String secondname = scanner.nextLine();
            System.out.println("PLEASE, ENTER YOUR NEW LASTNAME:");
            final String lastname = scanner.nextLine();
            userRepository.updateProfile(id, login, firstname, secondname, lastname);
            System.out.println("PROFILE UPDATED");
            userProfile(id);
            return 0;
        }
    }

    public int updateRole() {
        System.out.println("[UPDATE USER ROLE]");
        if (userRepository.findById(Application.userIdCurrent).isAdmin_true() == true) {
            System.out.println("PLEASE, ENTER UPDATE USER LOGIN:");
            final String login = scanner.nextLine();
            final User user = userRepository.findById(Application.userIdCurrent);
            if (user == null) {
                System.out.println("FAIL");
                return 0;
            }
            System.out.println("PLEASE, ENTER ROLE: ADMIN OR USER");
            final String role = scanner.nextLine();
            if (role.equals("ADMIN") || role.equals("USER")) {
                userRepository.updateRole(login, role);
                System.out.println("OK");
                return 0;
            } else {
                System.out.println("UNKNOWN ROLE!");
                return 0;
            }

        } else {
            System.out.println("THIS FUNCTIONALITY IS FOR ADMINS ONLY! FAIL.");
            return -1;
            }
        }
    public int updateUserByLogin(){
        System.out.println("[UPDATE USER]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        final User user = userRepository.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PASSWORD");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SECOND NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LASt NAME");
        final String lastName = scanner.nextLine();
        userRepository.updateByLogin(login, password, firstName, secondName, lastName);
        System.out.println("[OK]");
        return 0;
    }

    public int clearUsers() {
        System.out.println("[CLEAR USERS]");
        userRepository.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        final User user = userRepository.removeByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int listUsers() {
        System.out.println("[LIST USERS]");
        int index = 1;
        for (final User user: userRepository.findAll()) {
            System.out.println(index + ". " + user.getId() + " "  + user.getLogin() + ": " + user.getHashPassword() + " " + user.getUserRole());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER DETAIL]");
        System.out.println("USER ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD HASH: " + user.getHashPassword());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("SECOND NAME: " + user.getSecondName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getUserRole());
        System.out.println("[OK]");
    }

    public int viewUserByLogin() {
        System.out.println("ENTER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userRepository.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        this.viewUser(user);
        return 0;
    }

    public int viewUserById() {
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userRepository.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        this.viewUser(user);
        return 0;
    }

    public int updateUserById(){
        System.out.println("[UPDATE USER]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userRepository.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER ID:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SECOND NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String lastName = scanner.nextLine();
        userRepository.updateById(id, login, password, firstName, secondName, lastName);
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserById() {
        System.out.println("[REMOVE USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userRepository.removeById(id);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }




    public int userProfile(final Long id) {
        if (id == null) return -1;
        System.out.println("CURRENT SESSION:");
        System.out.println("ID:" + id.toString());
        System.out.println("LOGIN: " + userRepository.findById(id).getLogin());
        System.out.println("FIRSTNAME:" + userRepository.findById(id).getFirstName());
        System.out.println("SECONDNAME:" + userRepository.findById(id).getSecondName());
        System.out.println("LASTNAME:" + userRepository.findById(id).getLastName());
        System.out.println("ROLE:" + userRepository.findById(id).getUserRole());
        return 0;
    }

    public String isLoginExists() {
        System.out.println("PLEASE, ENTER YOUR NEW LOGIN:");
        final String login = scanner.nextLine();
        if (login == null || login.isEmpty()) {
            System.out.println("LOGIN MAST NOT BE EMPTY!");
            System.out.println("FAIL");
            return null;
        }
        if (userRepository.findByLogin(login) != null) {
            System.out.println("THIS LOGIN EXISTS!");
            System.out.println("FAIL");
            return null;
        }
        return login;
    }

}

