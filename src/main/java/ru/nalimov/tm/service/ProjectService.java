package ru.nalimov.tm.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nalimov.tm.Application;
import ru.nalimov.tm.entity.Project;
import ru.nalimov.tm.entity.User;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.repository.ProjectRepository;
import ru.nalimov.tm.repository.UserRepository;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class ProjectService extends AbstractController {

    private static final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private static final UserRepository userRepository = UserRepository.getInstance();

 //   private static final TaskRepository taskRepository = TaskRepository.getInstance();

    private final static Logger logger = LogManager.getLogger(ProjectService.class);

    private static ProjectService instance = null;


    public ProjectService() {
    }

    public static ProjectService getInstance() {
        synchronized (ProjectService.class) {
            return instance == null
                    ? instance = new ProjectService()
                    : instance;
        }
    }

    public Project create(String name, String description, Long userid) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
       // logger.trace("CREATE PROJECT " + name + " " + description + " " + userid);
        return projectRepository.create(name, description, userid);
    }

    public Project create(final String name, final UUID userId) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("CREATE PROJECT " + name);
        return projectRepository.create(name, userId);
    }

    public int createProject() {
        System.out.println("[CREATE_PROJECT]");
        logger.info("[CREATE PROJECT]");
        System.out.println("INPUT PROJECT NAME");
        final String name = scanner.nextLine();
        System.out.println("INPUT PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        create(name, description, Application.userIdCurrent);
        System.out.println("[OK]");
        return 0;
    }

    public Project update(Long id, String name, String description) throws ProjectNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        logger.trace("UPDATE PROJECT " + name + " " + description + " " + id);
        return projectRepository.update(id, name, description);
    }

    public int addProjectToUser() {
        System.out.println("ADDING PROJECT TO USER");
        logger.info("ADDING PROJECT TO USER");
        try {
            System.out.print("Enter project id: ");
            final Long projectId = Long.parseLong(scanner.nextLine());
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            final Project project = addProjectToUser(projectId, userId);
            System.out.println("Added user to project: " + project.toString());
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                addProjectToUser();
        }
        return 0;
    }

    public int updateProjectByIndex() throws ProjectNotFoundException {
        System.out.println("[UPDATE_PROJECT]");
        logger.info("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Project project = findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("INPUT PROJECT NAME");
        final String name = scanner.nextLine();
        System.out.println("INPUT PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectById() throws ProjectNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        logger.info("[UPDATE PROJECT BY ID]");
        System.out.println("INPUT PROJECT ID");
        final long id = Long.parseLong(scanner.nextLine());
        final Project project = findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION");
        final String description = scanner.nextLine();
        update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public Project findByIndex(int index) throws ProjectNotFoundException {
        if (index < 0) return null;
        return findByIndex(index);
    }

    public Project findById(Long id) throws ProjectNotFoundException {
        if (id == null) return null;
        return findById(id);
    }

    public int listProject() {
        logger.info("[LIST_PROJECT]");
        int index = 1;
        viewProjects(findAll());
        for (final Project project: findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int viewProjectsList(final Long userId) {
        System.out.println("VIEW PROJECTS");
        try {
            if (!checkPermission(userId))
                return 0;
        } catch (ProjectNotFoundException e) {
            e.printStackTrace();
        }
        viewProjects(findAll());
        return 0;
    }

    public int viewProjectListByUserId() {
        System.out.println("VIEW PROJECTS BY USERID");
        System.out.print("Enter user id: ");
        final Long userId = Long.parseLong(scanner.nextLine());
        viewProjects(findAllByUserId(userId));
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    public void viewProjects(final List<Project> projects) {
        int index = 1;
        if (projects == null || projects.isEmpty()) return;
        ProjectSortByName(projects);
        for (Project project : projects) {
            System.out.println("Project №" + index + project.getName());
            index++;
        }
    }

    public int viewProjectByIndex() throws ProjectNotFoundException {
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = scanner.nextInt() -1;
        final Project project = findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int viewProjectById() throws ProjectNotFoundException {
        System.out.println("ENTER, PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = findById(id);
        viewProject(project);
        return 0;
    }


    public Project removeById(Long id) throws ProjectNotFoundException {
        if (id == null) return null;
        logger.trace("REMOVE PROJECT BY ID " + id);
        return projectRepository.removeById(id);
    }

    public Project removeByIndex(int index) throws ProjectNotFoundException {
        if (index < 0 || index > projectRepository.size() -1) return null;
        if (projectRepository.findByIndex(index) == null) return null;
        if (projectRepository.findByIndex(index).getUserId().equals(Application.userIdCurrent)) return projectRepository.removeByIndex(index);
        else return null;

    }

    public int removeProjectById() throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY ID]");
        logger.info("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex() throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        logger.info("[REMOVE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final long index = scanner.nextLong();
        Project project = null;
        try {
            project = findByIndex((int) index);
            if (project == null) System.out.println("[FAIL]");
            removeByIndex((int) index);
        } catch (ProjectNotFoundException e) {
            logger.error(e);
            throw e;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int clear() {
        projectRepository.clear();
        return 0;
    }

    public int clearProject() {
        System.out.println("[CLEAR_PROJECT]");
        logger.info("[CLEAR_PROJECT]");
        projectRepository.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int clearProjectsByUserId() {
        System.out.println("CLEARING PROJECTS BY USERID");
        logger.info("CLEARING PROJECTS BY USERID");
        try {
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            projectRepository.clearProjectsByUserId(userId);
            System.out.println("Removed all projects with user id " + userId + " ");
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                return -1;
        }
        return 0;
    }

    public void clearProjectsByUserId(Long userId) throws ProjectNotFoundException {
        projectRepository.clearProjectsByUserId(userId);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project addProjectToUser(final Long projectId, final Long userId) throws ProjectNotFoundException {
        final Project project = findById(projectId);
        if (project == null)
            return null;
        project.setUserId(userId);
        return project;
    }

    public List<Project> findAllByUserId(Long userId) {
        return projectRepository.findAllByUserId(userId);
    }

    public void ProjectSortByName(List<Project> projects) {
        Collections.sort(projects, Project.ProjectSortByName);
    }

    private Boolean checkPermission(final Long userId) throws ProjectNotFoundException {
        if (userId == null) {
            System.out.println("NO PERMISSIONS");
            return false;
        }
        final User user = userRepository.findById(userId);
        if (!user.getUserRole().equals(Role.ADMIN)) {
            System.out.println("NO PERMISSIONS");
            return false;
        }
        return true;
    }

}
