package ru.nalimov.tm.publisher;

import ru.nalimov.tm.enumerated.Command;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.exceptions.TaskNotFoundException;
import ru.nalimov.tm.listener.CommandListener;
import ru.nalimov.tm.service.ProjectService;
import ru.nalimov.tm.service.SystemService;
import ru.nalimov.tm.service.TaskService;
import ru.nalimov.tm.service.UserService;

import java.util.ArrayList;
import java.util.List;

import static ru.nalimov.tm.constant.TerminalConst.*;

public class CommandManager {

    private final List<CommandListener> commandListeners = new ArrayList<>();

    private final SystemService systemService = SystemService.getInstance();

    public static Long id = null;

    public void register(final CommandListener commandListener) {
        commandListeners.add(commandListener);
    }

    public void unregister(final CommandListener commandListener) {
        commandListeners.remove(commandListener);
    }

    public void notify(final Command command) throws ProjectNotFoundException, TaskNotFoundException {
        for (CommandListener commandListener: commandListeners) {
            commandListener.execute(command);
        }

    }
    public static int run(final String param) throws TaskNotFoundException, ProjectNotFoundException  {
        if (param == null || param.isEmpty()) return -1;
        SystemService.getInstance().addCommandToHistory(param);
        switch (param) {
            case VERSION: return SystemService.getInstance().displayVersion();
            case ABOUT: return SystemService.getInstance().displayAbout();
            case HELP: return SystemService.getInstance().displayHelp();
            case EXIT: return SystemService.getInstance().displayExit();
            case COMMAND_HISTORY: return SystemService.getInstance().displayHistory();


            case PROJECT_CREATE: return ProjectService.getInstance().createProject();
            case PROJECT_CLEAR: return ProjectService.getInstance().clear();
            case PROJECT_LIST: return ProjectService.getInstance().listProject();
            case PROJECT_VIEW_BY_INDEX: return ProjectService.getInstance().viewProjectByIndex();
            case PROJECT_VIEW_BY_ID: return ProjectService.getInstance().viewProjectById();
            case PROJECT_REMOVE_BY_ID: return ProjectService.getInstance().removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return ProjectService.getInstance().removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return ProjectService.getInstance().updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID: return ProjectService.getInstance().updateProjectById();

            case TASK_CREATE: return TaskService.getInstance().createTask();
            case TASK_CLEAR: return TaskService.getInstance().clearTask();
            case TASK_LIST: return TaskService.getInstance().listTask();
            case TASK_VIEW_BY_INDEX: return TaskService.getInstance().viewTaskByIndex();
            case TASK_VIEW_BY_ID: return TaskService.getInstance().viewTaskById();
            case TASK_REMOVE_BY_ID: return TaskService.getInstance().removeTaskById();
            case TASK_REMOVE_BY_INDEX: return TaskService.getInstance().removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return TaskService.getInstance().updateTaskByIndex();
            case TASK_UPDATE_BY_ID: return TaskService.getInstance().updateTaskById();
            case TASK_ADD_PROJECT_BY_IDS: return TaskService.getInstance().addTaskToProjectByIds();
            case TASK_REMOVE_PROJECT_BY_IDS: return TaskService.getInstance().removeTaskProjectByIds();
            case TASK_LIST_BY_PROJECT_ID: return TaskService.getInstance().listTaskByProjectId();

            case TASK_ADD_TO_USER_BY_IDS: return TaskService.getInstance().addTaskToUser();
            case TASK_CLEAR_BY_USER_ID: return TaskService.getInstance().clearTasksByUserId();
            case TASK_LIST_BY_USER_ID: return TaskService.getInstance().viewTaskListByUserId();

            case USER_CREATE: return UserService.getInstance().createUser();
            case ADMIN_CREATE: return UserService.getInstance().createUser(Role.ADMIN);
            case USERS_CLEAR: return UserService.getInstance().clearUsers();
            case USERS_LIST: return UserService.getInstance().listUsers();
            case USER_VIEW_BY_LOGIN: return UserService.getInstance().viewUserByLogin();
            case USER_REMOVE_BY_LOGIN: return UserService.getInstance().removeUserByLogin();
            case USER_UPDATE_BY_LOGIN: return UserService.getInstance().updateUserByLogin();
            case USER_VIEW_BY_ID: return UserService.getInstance().viewUserById();
            case USER_REMOVE_BY_ID: return UserService.getInstance().removeUserById();
            case USER_UPDATE_BY_ID: return UserService.getInstance().updateUserById();
            case USER_AUTHENTIC: return UserService.getInstance().userAuthentic();
            case USER_DEAUTHENTIC: return UserService.getInstance().userDeauthentic();
            case USER_CHANGE_PASSWORD: return UserService.getInstance().userChangePassword();
            case USER_UPDATE_ROLE: return UserService.getInstance().updateRole();
            case USER_PROFILE_UPDATE: return UserService.getInstance().updateProfile(id);
            case USER_PROFILE_VIEW: return UserService.getInstance().userProfile(id);
            case PROJECT_ADD_TO_USER_BY_IDS: return ProjectService.getInstance().addProjectToUser();
            case PROJECT_CLEAR_BY_USER_ID: return ProjectService.getInstance().clearProjectsByUserId();
            case PROJECT_LIST_BY_USER_ID: return ProjectService.getInstance().viewProjectListByUserId();
            default: return SystemService.getInstance().displayErr();
        }
    }

}
