package ru.nalimov.tm.listener;

import ru.nalimov.tm.enumerated.Command;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.exceptions.TaskNotFoundException;

public interface CommandListener {
    void execute(Command command) throws ProjectNotFoundException, TaskNotFoundException;
}
