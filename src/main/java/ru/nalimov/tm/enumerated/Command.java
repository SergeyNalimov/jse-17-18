package ru.nalimov.tm.enumerated;

public enum Command {

    HELP("help", "Display list of terminal commands"),
    VERSION("version", "Display program version"),
    ABOUT("about", "Display developer info"),
    EXIT("exit", "Exit"),
    COMMAND_HISTORY("history", "View last 10 commsnds"),

    PROJECT_CREATE("project-create", "Create new project by name"),
    PROJECT_CLEAR("project-clear", "Remove all project"),
    PROJECT_LIST("project-list", "Display list of projects"),
    PROJECT_VIEW_BY_INDEX("project-view-by-index", "display project by index"),
    PROJECT_VIEW_BY_ID("project-view-by-id", "display project by id"),
    PROJECT_REMOVE_BY_NAME("project-remove-by-name", "delete project by name"),
    PROJECT_REMOVE_BY_ID("project-remove-by-id", "delete project by id"),
    PROJECT_REMOVE_BY_INDEX("project-remove-by-index", "delete project by index"),
    PROJECT_UPDATE_BY_INDEX("project-update-by-index", "update project by index"),
    PROJECT_UPDATE_BY_ID("project-update-by-id", "update project by id"),

    TASK_CREATE("task-create", "Create new task by name"),
    TASK_CLEAR("task-clear", "Remove all task"),
    TASK_LIST("task-list", "Display list of tasks"),
    TASK_VIEW_BY_INDEX("task-view-by-index", "display project by index"),
    TASK_VIEW_BY_ID("task-view-by-id", "display project by id"),
    TASK_REMOVE_BY_NAME("task-remove-by-name", "delete task by name"),
    TASK_REMOVE_BY_ID("task-remove-by-id", "delete task by id"),
    TASK_REMOVE_BY_INDEX("task-remove-by-index", "delete task by index"),
    TASK_UPDATE_BY_INDEX("task-update-by-index", "update task by index"),
    TASK_UPDATE_BY_ID("task-update-by-id", "update task by id"),
    TASK_LIST_BY_PROJECT_ID("task-list-by-project-id", "task list by project id"),
    TASK_ADD_PROJECT_BY_IDS("task-add-to-project-by-ids", "task add to project by ids"),
    TASK_REMOVE_PROJECT_BY_IDS("task-remove-from-project-by-ids", "task remove from project by ids"),

    USER_CREATE("user-create", "user create"),
    ADMIN_CREATE("admin-create", "admin create"),
    USERS_CLEAR("users-clear", "users clear"),
    USERS_LIST("users-list", "users list"),
    USER_VIEW_BY_LOGIN("user-view-by-login", "user view by login"),
    USER_REMOVE_BY_LOGIN("user-remove-by-login", "user remove by login"),
    USER_UPDATE_BY_LOGIN("user-update-by-login", "user update by login"),
    USER_VIEW_BY_ID("user-view-by-id", "user view by id"),
    USER_REMOVE_BY_ID("user-remove-by-id", "user remove by id"),
    USER_UPDATE_BY_ID("user-update-by-id", "user update by id"),
    USER_AUTHENTIC("user-authentic", "user authentic"),
    USER_DEAUTHENTIC("user-deauthentic", "user deauthentic"),
    USER_CHANGE_PASSWORD("user-change-password", "user change password"),
    USER_UPDATE_ROLE("user-update-role", "user update role"),
    USER_PROFILE_UPDATE("user-profile-update", "user profile update"),
    USER_PROFILE_VIEW("user-profile", "user profile"),
    TASK_ADD_TO_USER_BY_IDS("task-add-to-user-by-ids", "task add to user by ids"),
    TASK_CLEAR_BY_USER_ID("task-clear-by-user-id", "task clear by user id"),
    TASK_LIST_BY_USER_ID("task-list-by-user-id", "task list by user id"),
    PROJECT_ADD_TO_USER_BY_IDS("project-add-to-user-by-ids", "project add to user by ids"),
    PROJECT_CLEAR_BY_USER_ID("project-clear-by-user-id", "project clear by user id"),
    PROJECT_LIST_BY_USER_ID("project-list-by-user-id", "project list by user id");

    private final String name;

    private final String description;

    Command(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Command fromString(final String str) {
        for (Command command: Command.values()) {
            if (command.getName().equalsIgnoreCase(str))
                return command;
        }
        return null;
    }

}

