package ru.nalimov.tm.repository;

import ru.nalimov.tm.entity.User;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.util.HashMD5;

import java.util.ArrayList;
import java.util.List;



public class UserRepository {

    private static UserRepository instance = null;
    private final List<User> users = new ArrayList<>();
    private Long userAuthenticId;
    private Long userDeauthentic;

    public UserRepository() {
    }

    public static UserRepository getInstance() {
        if (instance == null) {
            synchronized(UserRepository.class) {
                if (instance == null)
                    instance = new UserRepository();
            }
        }
        return instance;
    }

    public User create(
            final String login, final String userPassword, final String firstName,
            final String secondName, final String lastName
    ){
        final User user = new User(login, userPassword, firstName, secondName, lastName);
        users.add(user);
        return user;
    }

    public User create(
            final String login, final String userPassword, final String firstName, final String secondName,
            final String lastName, Role userRole
    ){
        final User user = new User(login, userPassword, firstName, secondName, lastName, userRole);
        users.add(user);
        return user;
    }

    public User findByLogin(final String login) {
        for (final User user: users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateByLogin(
            final String login, final String userPassword, final String firstName, final String secondName,
            final String lastName
    ){
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setLastName(lastName);
        return user;
    }

    public User findById(final Long id) {
        if (id == null) return null;
        for (final User user: users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    public User removeById(final Long id) {
        User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateById(
            final Long id, final String login, final String userPassword, final String firstName,
            final String secondName, final String lastName) {
        User user = findById(id);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setLastName(lastName);
        return user;
    }

    public User updateProfile(final Long id, String login, String firstname, String secondname, String lastname) {
        final User user = findById(id);
        if (user == null) return null;
        user.setLogin(login);
        user.setFirstName(firstname);
        user.setSecondName(secondname);
        user.setLastName(lastname);
        return user;
    }

    public User updateRole(final String login, String role) {
        final User user = findByLogin(login);
        if (user == null) return null;
        switch (role) {
            case "ADMIN":
                user.setUserRole(Role.ADMIN);
            case "USER":
                user.setUserRole(Role.USER);
        }
        return user;
    }
    public User userAuthentic(String login, String userPassword) {
        User user = findByLogin(login);
        if (user == null) return null;
        if (!user.getHashPassword().equals(HashMD5.getHash(userPassword))) return null;
        this.setUserAuthenticId(user.getId());
        return user;
    }
    public void userDeauthentic(){
        this.userDeauthentic = null;
    }

    public void setUserAuthenticId(Long userAuthenticId) {
        this.userAuthenticId = userAuthenticId;
    }


    public void clear(){
        users.clear();
    }

    public List<User> findAll()  {
        return users;
    }

    public User userChangePassword(String login, String userOldPassword, String userNewPassword) {
        User user = findByLogin(login);
        if (user == null) return null;
        if (!user.getHashPassword().equals(HashMD5.getHash(userOldPassword))) return null;
        user.setHashPassword(userNewPassword);
        return user;
    }

}
