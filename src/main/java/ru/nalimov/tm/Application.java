package ru.nalimov.tm;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.exceptions.TaskNotFoundException;
import ru.nalimov.tm.listener.TMCommandListener;
import ru.nalimov.tm.publisher.CommandManager;
import ru.nalimov.tm.service.*;

import java.util.Scanner;

import static ru.nalimov.tm.constant.TerminalConst.*;

public class Application {

    private final SystemService systemService = SystemService.getInstance();

    public static Long id = null;

    public static Long userIdCurrent = null;

    private static final Logger logger = LogManager.getLogger(Application.class.getName());

    {
        UserService.getInstance().create("TEST","pass1","Фам1","Имя1","Отч1");
        UserService.getInstance().create("ADMIN","pass2","Фам2","Имя2","Отч2",Role.ADMIN);
        ProjectService.getInstance().create("DEMO_PROJECT_3", "DESC PROJECT 3", UserService.getInstance().findByLogin("ADMIN").getId());
        ProjectService.getInstance().create("DEMO_PROJECT_2", "DESC PROJECT 2", UserService.getInstance().findByLogin("TEST").getId());
        TaskService.getInstance().create("TEST_TASK_2", "DESC TASK 2", UserService.getInstance().findByLogin("TEST").getId());
        TaskService.getInstance().create("TEST_TASK_1", "DESC TASK 1", UserService.getInstance().findByLogin("TEST").getId());
    }

    public static void main(final String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Application app = new Application();
        app.systemService.displayWelcome();

        final CommandManager commandManager = new CommandManager();
        commandManager.register(new TMCommandListener());
          String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            try {
                CommandManager.run(command);
            } catch (ProjectNotFoundException | TaskNotFoundException e) {
                //logger.error(e);
                e.getMessage();
            }
        }
    }
}
